# 在线词频查询工具

#### 介绍

1. 在线词频查询工具，已经上线。
2.  **项目文件在分支: 1.0** 


#### 软件架构
使用了maven管理项目包


#### 安装教程

1.  JDK版本： 1.8
2.  数据库版本： MySQL 8
3.  数据库创建SQL语句：（或者在 项目目录下的 .sql 文件中查看）  
-     create database word_frequency_detection;
-     use word_frequency_detection;
-     create table advice(a_id int auto_increment primary key, a_advice varchar(225));
-     create table word(w_id int auto_increment primary key, w_word char(50) , w_chinese char(255), w_occurence_num int);


4. 创建项目所需的路径
-     如果为 linux
-    /product/word_frequency_detection/input/
-    /product/word_frequency_detection/output/
-     如果为 window
-    C:\\Users\\MACHENIKE\\Desktop\\word\\input\\
-    C:\\Users\\MACHENIKE\\Desktop\\word\\output\\

在不同环境运行需要更改的值的路径；

1. mysql配置文件：
 \WordFrequencyDetection\src\main\resources\SqlMapConfig.xml
2. 默认输入输出文件：
 \WordFrequencyDetection\src\main\java\com\zhz\servlet\UploadServlet.java
 

 **注意： 项目所使用的  mysql-connector-java-8.0.12.jar  ,该版本maven工具可能不能正常添加，需要手动添加包依赖，所用的 .jar 文件放在项目目录->工具目录->lib中。** 

#### 使用说明

 首次环境搭建好，数据库创建好，能够访问后，还不能直接进行词频查询或者单词查询工作，需要执行以下步骤：
  
1. 在访问的路径后面添加 /update.jsp  访问到数据库数据更新的页面
2. 进行数据的写入： 点击页面中文件选择框，选择 项目目录->工具目录->添加词库所用.txt，点击上传，等待跳转到主页后，便是数据写入成功
3. 在主页，点击文件选择框，选择 项目目录->工具目录->测试词频查询所用.txt，点击”确定上传“，如果能够正常跳转到结果页，即为项目正常运行


